const testNumber = document.querySelectorAll('.test-number'); 
const timer = document.querySelector('.timer')



function getTestNumber(){
    for(i=0; i<testNumber.length; i++){
        testNumber[i].innerHTML=i + 1 + '. '
    }
};

getTestNumber();

let sec = 0;
let min = 0
let secValue
let minValue



timer.innerHTML= '00 : 00';

setInterval(function(){
    if(min>=40 && sec>0){
        return
    }
    if (sec<10){
        secValue = '0'+sec
    }
    else {
        secValue=sec
    }
    if (min<10){
        minValue = '0'+min
    }
    else {
        minValue=min
    }
    timer.innerHTML= minValue + " : "+ secValue
    sec++
    if(sec>=60){
        sec=0
        min++
    }
}, 1000);

